from odoo import http
from odoo.http import request


# define two controller first controller for rendering(displaying) page

class CreateRequest(http.Controller):

    @http.route('/user/webform', auth='public', website=True, type="http")
    def user_webform(self, **kw):
        # to manage displaying many2one fields (unless it it will display as char)
        requests = request.env['maintenance.request'].sudo().search([])
        equipment_ids = request.env['maintenance.equipment'].sudo().search([])
        maintenance_team_ids = request.env['maintenance.team'].sudo().search([])
        return http.request.render('request.create_user',
                                   {'requests': requests,
                                    'equipment_ids': equipment_ids,
                                    'maintenance_team_ids': maintenance_team_ids, })

    # second controller controller for creating record in odoo backend
    @http.route('/create/webUser', auth='public', website=True, type="http")
    def create_webuser(self, **kw):
        """  Create maintenance ticket ,call form fields"""
        values = kw
        try:
            new_user = request.env['maintenance.request'].sudo().create(
                {'name': values['name'], 'equipment_id': int(values['equipment_id']),
                 'maintenance_team_id': int(values['maintenance_team_id']),
                 'maintenance_type': values['maintenance_type'],
                 'schedule_date': values['schedule_date'],
                 })
            return http.request.render('request.user_thanks_page',
                                       {'title': str(new_user.name)})
        except:
            print("Please refresh the page")


    @http.route('/user/webdisplay/', website=True, auth="public")
    def display_request(self, **kw):
        requests = request.env[('maintenance.request')].sudo().search([])
        return request.render("request.display_maintenance_request", {
            'requests': requests,

        })

    @http.route('/web_maintenance/card/', website=True, auth="public")
    def card_request(self, **kw):
        id=kw.get('id')
        cards = request.env['maintenance.request'].sudo().search([('id','=',id)])
        return request.render("request.card_request", {
            'cards': cards,

        })
